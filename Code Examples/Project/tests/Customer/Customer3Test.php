<?php
/**
 * @copyright 	2016 Noah Heck
 * @since 		2016-09-05
 */

namespace Customer\Test;

use PHPUnit_Framework_TestCase as TestCase;
use UTWP\Customer\Customer3 as Customer;

class Customer3Test extends TestCase
{
	private function getEmailValidator()
	{
		return $this->getMockBuilder("UTWP\Email\Validator")
					->disableOriginalConstructor()
					->getMock();
	}



	public function testGetEmailReturnsEmptyIfNoEmailProvided()
	{
		$emailValidator = $this->getEmailValidator();

		$details = [
			  "customerId" => "1"
			, "name"       => "Noah Heck"
			, "address"    => "1341 Fillmore St. Suite 200"
			, "city"       => "Twin Falls"
			, "state"      => "ID"
			, "zip"        => "83301"
			, "email"      => ""
		];

		$customer = new Customer($emailValidator, $details);

		$expected = "";

		$actual   = $customer->getEmail();

		$this->assertEquals($expected, $actual);
	}



	public function testGetEmailReturnsEmptyIfInvalidEmailProvided()
	{
		$emailValidator = $this->getEmailValidator();

		$emailValidator->method("isValid")
						->willReturn(false);

		$details = [
			"email" => "something_doesn't_matter_what_it_is"
		];

		$customer = new Customer($emailValidator, $details);

		$expected = "";

		$actual   = $customer->getEmail();

		$this->assertEquals($expected, $actual);
	}



	public function testGetEmailReturnsEmailIfValidEmailProvided()
	{
		$emailValidator = $this->getEmailValidator();

		$emailValidator->method("isValid")
						->willReturn(true);

		$details = [
			"email" => "something_doesn't_matter_what_it_is"
		];

		$customer = new Customer($emailValidator, $details);

		$expected = "something_doesn't_matter_what_it_is";

		$actual   = $customer->getEmail();

		$this->assertEquals($expected, $actual);
	}
}
