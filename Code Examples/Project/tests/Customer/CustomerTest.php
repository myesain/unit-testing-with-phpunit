<?php
/**
 * @copyright 	2016 Noah Heck
 * @since 		2016-09-05
 */

namespace Customer\Test;

use PHPUnit_Framework_TestCase as TestCase;
use UTWP\Customer\Customer;

class CustomerTest extends TestCase
{
	public function testGetEmailReturnsEmptyIfNoEmailProvided()
	{
		$details = [
			  "customerId" => "1"
			, "name"       => "Noah Heck"
			, "address"    => "1341 Fillmore St. Suite 200"
			, "city"       => "Twin Falls"
			, "state"      => "ID"
			, "zip"        => "83301"
			, "email"      => ""
		];

		$customer = new Customer($details);

		$expected = "";

		$actual   = $customer->getEmail();

		$this->assertEquals($expected, $actual);
	}



	public function testGetEmailReturnsEmailIfValidEmailProvided()
	{
		$details = [
			  "customerId" => "1"
			, "name"       => "Noah Heck"
			, "address"    => "1341 Fillmore St. Suite 200"
			, "city"       => "Twin Falls"
			, "state"      => "ID"
			, "zip"        => "83301"
			, "email"      => "noah@bestnotes.com"
		];

		$customer = new Customer($details);

		$expected = "noah@bestnotes.com";

		$actual   = $customer->getEmail();

		$this->assertEquals($expected, $actual);
	}



	public function testGetEmailReturnsEmptyIfInvalidEmailProvided()
	{
		$details = [
			  "customerId" => "1"
			, "name"       => "Noah Heck"
			, "address"    => "1341 Fillmore St. Suite 200"
			, "city"       => "Twin Falls"
			, "state"      => "ID"
			, "zip"        => "83301"
			, "email"      => "bad|email@something.com"
		];

		$customer = new Customer($details);

		$expected = "";

		$actual   = $customer->getEmail();

		$this->assertEquals($expected, $actual);
	}



	public function testGetEmailReturnsEmptyIfInvalidDomainProvided()
	{
		$details = [
			  "customerId" => "1"
			, "name"       => "Noah Heck"
			, "address"    => "1341 Fillmore St. Suite 200"
			, "city"       => "Twin Falls"
			, "state"      => "ID"
			, "zip"        => "83301"
			, "email"      => "info@safe.pharmacy"
		];

		$customer = new Customer($details);

		$expected = "";

		$actual   = $customer->getEmail();

		$this->assertEquals($expected, $actual);
	}
}
