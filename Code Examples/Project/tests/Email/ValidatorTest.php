<?php
/**
 * @copyright 2016 Noah Heck
 * @since     2016-09-05
 */

namespace UTWP\Test\Email;

use PHPUnit_Framework_TestCase as TestCase;
use UTWP\Email\Validator;

class ValidatorTest extends TestCase
{
	private $invalidEmails = [
		  "no.domain@invalid"
		, "apostrophe'in'email@something.com"
		, "comma,in,email@test.com"
		, "firstName.lastName@invalid.domain"
	];

	private $validEmails = [
		  "noah@bestnotes.com"
		, "jayte@bestnotes.com"
		, "team@bestnotes.com"
		, "rvannoy@csi.edu"
	];

	public function testIsValidReturnsFalseIfNoValueProvided()
	{
		$validator = new Validator();

		$this->assertFalse($validator->isValid(""));
	}

	public function testIsValidReturnsFalseIfInvalidEmailProvided()
	{
		$validator = new Validator();

		foreach ($this->invalidEmails as $email) {
			$this->assertFalse($validator->isValid($email));
		}
	}

	public function testIsValidReturnsTrueIfValidEmailProvided()
	{
		$validator = new Validator();

		foreach ($this->validEmails as $email) {
			$this->assertTrue($validator->isValid($email));
		}
	}
}
