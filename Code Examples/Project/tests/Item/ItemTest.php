<?php
/**
 * @copyright 	2016 Noah Heck
 * @since 		2016-09-05
 */

namespace UTWP\Test\Item;

use PHPUnit_Framework_TestCase as TestCase;

use UTWP\Item\Item;

class ItemTest extends TestCase
{
	public function testGetTaxableAmountReturnsCorrectAmountWhenTaxable()
	{
		$details = [
			  "itemId"      => "1"
			, "name"        => "The Pragmatic Programmer"
			, "description" => "Good book for programmers"
			, "cost"        => "49.99"
			, "taxable"     => "1"
		];

		$item = new Item($details);

		$expected = 49.99;

		$actual   = $item->getTaxableAmount();

		$this->assertEquals($expected, $actual);
	}



	public function testGetTaxableAmountReturnsZeroWhenNotTaxable()
	{
		$details = [
			  "itemId"      => "1"
			, "name"        => "The Pragmatic Programmer"
			, "description" => "Good book for programmers"
			, "cost"        => "49.99"
			, "taxable"     => "0"
		];

		$item = new Item($details);

		$expected = 0;

		$actual   = $item->getTaxableAmount();

		$this->assertEquals($expected, $actual);
	}



	public function testPropertiesGetSetWithCorrectValuesProvidedToConstructor()
	{
		$details = [
			  "itemId"      => "1"
			, "name"        => "The Pragmatic Programmer"
			, "description" => "Good book for programmers"
			, "cost"        => "49.99"
			, "taxable"     => "0"
		];

		$item = new Item($details);

		$this->assertEquals($details['itemId']     , $item->itemId);
		$this->assertEquals($details['name']       , $item->name);
		$this->assertEquals($details['description'], $item->description);
		$this->assertEquals($details['cost']       , $item->cost);
		$this->assertEquals($details['taxable']    , $item->taxable);
	}
}
