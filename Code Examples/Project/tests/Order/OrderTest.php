<?php
/**
 * @copyright 	2016 Noah Heck
 * @since 		2016-09-05
 */

namespace UTWP\Test\Order;

use PDO;

use PHPUnit_Framework_TestCase as TestCase;

use UTWP\Order\Order;
use UTWP\Customer\Customer;
use UTWP\Item\Item;

class OrderTest extends TestCase
{
	public function testProcessOrderReturnsTrueAndSendsVerificationEmailIfOrderRecordedCorrectly()
	{
		$db = $this->getMockBuilder("UTWP\Database\MockDatabase")
					->getMock();



		$customer = $this->getMockBuilder("UTWP\Customer\Customer")
					->disableOriginalConstructor()
					->getMock();

		$customer->expects($this->once())
				->method("getEmail")
				->willReturn("noah@bestnotes.com");

		$customer->customerId = 101;



		$mailer = $this->getMockBuilder("UTWP\Email\Mailer")
						->getMock();
		$mailer->expects($this->once())
				->method("sendEmail")
				->with(
					  "noah@bestnotes.com"
					, Order::CONFIRMATION_EMAIL
					, $this->anything()
				)
				->willReturn(true);



		$stmt1 = $this->getMockBuilder("UTWP\Database\MockDatabaseStatement")
					->getMock();

		$stmt1->expects($this->exactly(3))
				->method("bindParam")
				->withConsecutive(
					  array($this->stringContains("orderTotal"), 45.99        , PDO::PARAM_STR)
					, array($this->stringContains("customerId"), 101          , PDO::PARAM_INT)
					, array($this->stringContains("orderDate" ), date("Y-m-d"), PDO::PARAM_STR)
				);

		$stmt1->expects($this->once())
				->method("execute")
				->willReturn(true);



		$stmt2 = $this->getMockBuilder("UTWP\Database\MockDatabaseStatement")
					->getMock();

		$stmt2->expects($this->exactly(3))
				->method("bindParam")
				->withConsecutive(
					  array($this->stringContains("itemId") , $this->anything(), PDO::PARAM_INT)
					, array($this->stringContains("cost")   , $this->anything(), PDO::PARAM_STR)
					, array($this->stringContains("taxable"), $this->anything(), PDO::PARAM_INT)
				);

		$stmt2->expects($this->once())
				->method("execute")
				->willReturn(true);



		$db->expects($this->once())
			->method("lastInsertId")
			->willReturn(123);

		$db->expects($this->once())
			->method("beginTransaction")
			->willReturn(true);

		$db->expects($this->once())
			->method("commit")
			->willReturn(true);



		$db->expects($this->exactly(2))
			->method("prepare")
			->will(
				$this->onConsecutiveCalls(
					  $stmt1
					, $stmt2
				)
			);

		$item = new Item([
			  'itemId'      => "1"
			, 'name'        => "Flowers for my wife"
			, 'description' => "Flowers"
			, 'cost'        => "45.99"
			, 'taxable'     => "1"
		]);



		$order = new Order($db, $customer, $mailer);

		$order->addItem($item);

		$this->assertTrue($order->processOrder());
	}
}
