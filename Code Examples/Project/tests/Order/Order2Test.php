<?php
/**
 * @copyright 	2016 Noah Heck
 * @since 		2016-09-05
 */

namespace UTWP\Test\Order;

use PDO;

use PHPUnit_Framework_TestCase as TestCase;

use UTWP\Order\Order2 as Order;
use UTWP\Customer\Customer;
use UTWP\Item\Item;

class Order2Test extends TestCase
{
	protected function getGateway()
	{
		return $this->getMockBuilder("UTWP\Order\Gateway\Order2Gateway")
					->disableOriginalConstructor()
					->getMock();
	}

	protected function getCustomer()
	{
		return $this->getMockBuilder("UTWP\Customer\Customer")
					->disableOriginalConstructor()
					->getMock();
	}

	protected function getMailer()
	{
		return $this->getMockBuilder("UTWP\Email\Mailer")
					->getMock();
	}

	public function testProcessOrderReturnsTrueAndSendsVerificationEmailIfOrderRecordedAndOrderItemsRecordedAndCustomerHasEmailAddress()
	{
		$gateway = $this->getGateway();

		$gateway->expects($this->once())
				->method("createOrder")
				->with(
					  45.99
					, 101
					, date("Y-m-d")
				)
				->willReturn(123);

		$gateway->expects($this->once())
				->method("recordOrderItems")
				->with([
					[123, "1", 45.99, 1]
				])
				->willReturn(true);

		$gateway->expects($this->once())
				->method("beginTransaction");

		$gateway->expects($this->once())
				->method("commit");


		$customer = $this->getCustomer();

		$customer->expects($this->once())
				->method("getEmail")
				->willReturn("noah@bestnotes.com");

		$customer->customerId = 101;



		$mailer = $this->getMailer();

		$mailer->expects($this->once())
				->method("sendEmail")
				->with(
					  "noah@bestnotes.com"
					, Order::CONFIRMATION_EMAIL
					, $this->anything()
				)
				->willReturn(true);

		$item = new Item([
			  'itemId'      => "1"
			, 'name'        => "Flowers for my wife"
			, 'description' => "Flowers"
			, 'cost'        => "45.99"
			, 'taxable'     => "1"
		]);



		$order = new Order($gateway, $customer, $mailer);

		$order->addItem($item);

		$this->assertTrue($order->processOrder());
	}

	public function testProcessOrderRollsBackTransactionAndReturnsFalseIfOrderNotCreated()
	{
		$gateway = $this->getGateway();

		$gateway->expects($this->once())
				->method("createOrder")
				->willReturn(false);

		$gateway->expects($this->once())
				->method("beginTransaction");

		$gateway->expects($this->once())
				->method("rollback");

		$customer = $this->getCustomer();
		$customer->customerId = 101;

		$mailer   = $this->getMailer();

		$item = new Item([
			  'itemId'      => "1"
			, 'name'        => "Flowers for my wife"
			, 'description' => "Flowers"
			, 'cost'        => "45.99"
			, 'taxable'     => "1"
		]);

		$order = new Order($gateway, $customer, $mailer);

		$order->addItem($item);

		$this->assertFalse($order->processOrder());
	}
}
