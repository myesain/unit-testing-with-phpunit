<?php
/**
 * @copyright 	2016 Noah Heck
 * @since 		2016-09-05
 */

namespace UTWP\Database;

use PDOStatement;

class MockDatabaseStatement extends PDOStatement
{
	/**
	 * Empty constructor - Used to make mocking PDO instances manageable
	 */
	public function __construct()
	{

	}
}
