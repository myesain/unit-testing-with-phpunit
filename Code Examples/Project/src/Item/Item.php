<?php
/**
 * @copyright 	2016 Noah Heck
 * @since 		2016-09-05
 */

namespace UTWP\Item;

class Item
{
	public $itemId;
	public $name;
	public $description;
	public $cost;
	public $taxable;

	public function __construct(array $details)
	{
		$this->itemId      = $details['itemId'];
		$this->name        = $details['name'];
		$this->description = $details['description'];
		$this->cost        = $details['cost'];
		$this->taxable     = $details['taxable'];
	}

	public function getTaxableAmount()
	{
		if (!$this->taxable) {
			return 0;
		}

		return $this->cost;
	}
}
