<?php
/**
 * @copyright 	2016 Noah Heck
 * @since 		2016-09-05
 */

namespace UTWP\Email;

class Validator
{
	public function isValid($value)
	{
		if (!preg_match("/^[\w\.]+@[\w\.]+\.[\w]{2,4}$/", $value)) {
			return false;
		}

		return true;
	}
}
