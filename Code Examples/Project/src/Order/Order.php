<?php
/**
 * @copyright 	2016 Noah Heck
 * @since 		2016-09-05
 */

namespace UTWP\Order;

use PDO;
use UTWP\Customer\Customer;
use UTWP\Email\Mailer;
use UTWP\Item\Item;

class Order
{
	const CONFIRMATION_EMAIL = "orders@example.com";

	private $db;
	private $customer;
	private $mailer;

	private $items = array();

	private $orderId;



	public function __construct(PDO $db, Customer $customer, Mailer $mailer)
	{
		$this->db       = $db;
		$this->customer = $customer;
		$this->mailer   = $mailer;
	}



	public function addItem(Item $item)
	{
		$this->items[] = $item;
	}

	public function getOrderTotal()
	{
		return $this->getTaxableSubtotal() + $this->getNonTaxableSubtotal();
	}

	/**
	 * Records the order and order items in the database, then sends verification email to customer
	 * @return bool
	 */
	public function processOrder()
	{
		$this->db->beginTransaction();



		$orderTotal = $this->getOrderTotal();
		$customerId = $this->customer->customerId;
		$orderDate  = date("Y-m-d");

		$query = "INSERT INTO orders
						SET   orderTotal = :orderTotal
							, customerId = :customerId
							, orderDate  = :orderDate";

		$stmt = $this->db->prepare($query);

		$stmt->bindParam(":orderTotal", $orderTotal, PDO::PARAM_STR);
		$stmt->bindParam(":customerId", $customerId, PDO::PARAM_INT);
		$stmt->bindParam(":orderDate" , $orderDate , PDO::PARAM_STR);

		if (!$stmt->execute()) {
			$this->db->rollBack();

			return false;
		}



		$this->orderId = $this->db->lastInsertId();



		try
		{
			$itemId  = "";
			$cost    = "";
			$taxable = "";

			$query = "INSERT INTO order_items
							SET   orderId = {$this->orderId}
								, itemId  = :itemId
								, cost    = :cost
								, taxable = :taxable";

			$stmt = $this->db->prepare($query);

			$stmt->bindParam(":itemId" , $itemId , PDO::PARAM_INT);
			$stmt->bindParam(":cost"   , $cost   , PDO::PARAM_STR);
			$stmt->bindParam(":taxable", $taxable, PDO::PARAM_INT);

			foreach ($this->items as $item) {
				$itemId  = $item->itemId;
				$cost    = $item->cost;
				$taxable = ($item->taxable) ? 1 : 0;

				if (!$stmt->execute()) {
					$this->db->rollBack();

					return false;
				}
			}
		}
		catch (\Exception $e)
		{
			$this->db->rollBack();

			return false;
		}



		$this->db->commit();



		$email = $this->customer->getEmail();

		if ($email) {
			$emailContent = "Thanks for submitting your order. [...]";

			$this->mailer->sendEmail($email, self::CONFIRMATION_EMAIL, $emailContent);
		}

		return true;
	}



	private function getTaxableSubtotal()
	{
		$taxableAmount = 0;

		foreach ($this->items as $item) {
			$taxableAmount += $item->getTaxableAmount();
		}

		return $taxableAmount;
	}

	private function getNonTaxableSubtotal()
	{
		$subtotal = 0;

		foreach ($this->items as $item) {
			if (!$item->taxable) {
				$subtotal += $item->cost;
			}
		}

		return $subtotal;
	}

}
