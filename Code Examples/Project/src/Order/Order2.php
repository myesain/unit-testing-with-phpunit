<?php
/**
 * @copyright 	2016 Noah Heck
 * @since 		2016-09-05
 */

namespace UTWP\Order;

use PDO;
use UTWP\Customer\Customer;
use UTWP\Email\Mailer;
use UTWP\Item\Item;

class Order2
{
	const CONFIRMATION_EMAIL = "orders@example.com";

	private $gateway;
	private $customer;
	private $mailer;

	private $items = array();

	private $orderId;



	public function __construct($gateway, Customer $customer, Mailer $mailer)
	{
		$this->gateway  = $gateway;
		$this->customer = $customer;
		$this->mailer   = $mailer;
	}



	public function addItem(Item $item)
	{
		$this->items[] = $item;
	}

	public function getOrderTotal()
	{
		return $this->getTaxableSubtotal() + $this->getNonTaxableSubtotal();
	}



	/**
	 * Records the order and order items in the database, then sends verification email to customer
	 * @return bool
	 */
	public function processOrder()
	{
		$this->gateway->beginTransaction();



		$orderTotal = $this->getOrderTotal();
		$customerId = $this->customer->customerId;
		$orderDate  = date("Y-m-d");

		$this->orderId = $this->gateway->createOrder($orderTotal, $customerId, $orderDate);

		if (!$this->orderId) {
			$this->gateway->rollBack();

			return false;
		}



		$values = [];
		foreach ($this->items as $item) {
			$values[] = [
				  $this->orderId
				, $item->itemId
				, $item->cost
				, $item->taxable
			];
		}

		if (!$this->gateway->recordOrderItems($values)) {
			$this->gateway->rollback();

			return false;
		}



		$this->gateway->commit();



		$email = $this->customer->getEmail();

		if ($email) {
			$emailContent = "Thanks for submitting your order. [...]";

			$this->mailer->sendEmail($email, self::CONFIRMATION_EMAIL, $emailContent);
		}

		return true;
	}



	private function getTaxableSubtotal()
	{
		$taxableAmount = 0;

		foreach ($this->items as $item) {
			$taxableAmount += $item->getTaxableAmount();
		}

		return $taxableAmount;
	}

	private function getNonTaxableSubtotal()
	{
		$subtotal = 0;

		foreach ($this->items as $item) {
			if (!$item->taxable) {
				$subtotal += $item->cost;
			}
		}

		return $subtotal;
	}

}
