<?php
/**
 * @copyright 	2016 Noah Heck
 * @since 		2016-09-05
 */

namespace UTWP\Order\Gateway;

use PDO;

/**
 * @codeCoverageIgnore
 */
class Order2Gateway
{
	private $db;

	private $wasInTransaction;

	public function __construct($db)
	{
		$this->db = $db;
	}

	public function beginTransaction()
	{
		if ($this->db->inTransaction()) {
			$this->wasInTransaction = true;
			return;
		}

		$this->db->beginTransaction();
	}

	public function commit()
	{
		if ($this->wasInTransaction) {
			return;
		}

		$this->db->commit();
	}

	public function rollback()
	{
		$this->db->rollback();
	}

	public function createOrder($orderTotal, $customerId, $orderDate)
	{
		$query = "INSERT INTO orders
						SET   orderTotal = :orderTotal
							, customerId = :customerId
							, orderDate  = :orderDate";

		$stmt = $this->db->prepare($query);

		$stmt->bindParam(":orderTotal", $orderTotal, PDO::PARAM_STR);
		$stmt->bindParam(":customerId", $customerId, PDO::PARAM_INT);
		$stmt->bindParam(":orderDate" , $orderDate , PDO::PARAM_STR);

		if (!$stmt->execute()) {
			return false;
		}

		return $this->db->lastInsertId();
	}

	public function recordOrderItems($values)
	{
		$placeholder = "(?, ?, ?, ?)";

		$numValues = count($values);

		$placeholders = implode(",", array_fill(0, $numValues, $placeholder));

		$values = array_reduce($values, function($container, $array) {
			return array_merge($container, $array);
		}, []);

		$query = "INSERT INTO order_items (orderId, itemId, cost, taxable)
						VALUES $placeholders";

		$stmt = $this->db->prepare($query);

		if (!$stmt->execute($values)) {
			return false;
		}

		return true;
	}
}
