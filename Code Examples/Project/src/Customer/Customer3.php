<?php
/**
 * @copyright 	2016 Noah Heck
 * @since 		2016-09-05
 */

namespace UTWP\Customer;

use UTWP\Email\Validator as EmailValidator;

class Customer3
{
	private $emailValidator;

	public $customerId;
	public $name;
	public $address;
	public $city;
	public $state;
	public $zip;
	public $email;

	public function __construct($emailValidator, $details)
	{
		$this->emailValidator = $emailValidator;

		$this->customerId = $details['customerId'];
		$this->name       = $details['name'];
		$this->address    = $details['address'];
		$this->city       = $details['city'];
		$this->state      = $details['state'];
		$this->zip        = $details['zip'];
		$this->email      = $details['email'];
	}

	/**
	 * Provides the customer's email address if they have a valid email address on file
	 *
	 * @return string
	 */
	public function getEmail()
	{
		if (!$this->email) {
			return "";
		}

		if (!$this->emailValidator->isValid($this->email)) {
			return "";
		}

		return $this->email;
	}
}
