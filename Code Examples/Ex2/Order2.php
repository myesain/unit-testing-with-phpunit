<?php
/**
 * @copyright 	2016 Noah Heck
 * @since 		2016-09-02
 */

class Order
{
	public function calculateSalesTax($taxRate, array $items)
	{
		$orderTotal = array_sum(array_column($items, "cost"));

		return round($taxRate * $orderTotal, 2);
	}
}
