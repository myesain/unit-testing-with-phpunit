<?php
/**
 * @copyright 	2016 Noah Heck
 * @since 		2016-09-02
 */

class Order
{
	public function calculateSalesTax($taxRate, array $items)
	{
		$taxAmount  = 0;
		$orderTotal = 0;

		foreach ($items as $item) {
			$orderTotal += $item['cost'];
		}

		$taxAmount = round($taxRate * $orderTotal, 2);

		return $taxAmount;
	}
}
