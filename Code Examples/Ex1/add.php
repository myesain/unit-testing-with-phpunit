<?php
/**
 * @copyright 	2016 Noah Heck
 * @since 		2016-09-02
 */

function add($a, $b) {
	$total = $a + $b;

	return $total;
}
